from fabric import task
from fabric import Connection


@task(optional=['service_name', 'port', 'image', 'gitlab_deploy_user', 'gitlab_deploy_token'])
def deploy(conn, service_name=None, port=None, image=None, gitlab_deploy_user=None, gitlab_deploy_token=None):
    if service_name is not None or image is not None or gitlab_deploy_user is not None or gitlab_deploy_token is not None:
        auth = f'echo {gitlab_deploy_token} | docker login -u {gitlab_deploy_user} --password-stdin registry.gitlab.com'
        pull = f'docker pull {image}'
        running_container = f'docker ps -a -q --filter name={service_name}' + ' --format="{{.ID}}"'
        start = f'docker run -d -p {port}:{port} --name={service_name} {image}'
        conn.run(f'{auth} && {pull}', warn=True)
        running_container_id = conn.run(running_container, warn=True)
        if running_container_id.stdout:
            stop = f'docker rm $(docker stop {running_container_id.stdout})'
            conn.run(stop, warn=True)
        conn.run(start, warn=True)
    else:
        raise Exception('provide all the arguments')


@task
def pruneunusedimages(conn):
    conn.run('docker image prune -a -f', warn=True)
