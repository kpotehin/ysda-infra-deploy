import os
import datetime

from django.http.response import HttpResponse

env = os.getenv('ENVIRONMENT')


def index(request):
    return HttpResponse(f'Hey, world! I\'m gunicorn worker from {env} environment. Current time: {datetime.datetime.now()}')
